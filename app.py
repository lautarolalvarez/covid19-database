from pandas import read_csv, concat
from datetime import datetime
from pymongo import MongoClient


class CoVid19Getter(object):
    def __init__(self):
        super(CoVid19Getter, self).__init__()
        self.baseUrl = ('https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/' +
                        'csse_covid_19_data/csse_covid_19_time_series/')
        self.confirmedFile = 'time_series_19-covid-Confirmed.csv'
        self.deathsFile = 'time_series_19-covid-Deaths.csv'
        self.recoveredFile = 'time_series_19-covid-Recovered.csv'
        self.columnNames = {
            'province': 'Province/State',
            'country': 'Country/Region',
            'latitude': 'Lat',
            'longitude': 'Long'
        }
        self.columns = ['province', 'country', 'latitude', 'longitude']
        self.confirmed = read_csv(self.baseUrl + self.confirmedFile).rename(columns={
            self.columnNames['country']: 'country',
            self.columnNames['province']: 'province',
            self.columnNames['latitude']: 'latitude',
            self.columnNames['longitude']: 'longitude',
        })
        self.deaths = read_csv(self.baseUrl + self.deathsFile).rename(columns={
            self.columnNames['country']: 'country',
            self.columnNames['province']: 'province',
            self.columnNames['latitude']: 'latitude',
            self.columnNames['longitude']: 'longitude',
        })
        self.recovered = read_csv(self.baseUrl + self.recoveredFile).rename(columns={
            self.columnNames['country']: 'country',
            self.columnNames['province']: 'province',
            self.columnNames['latitude']: 'latitude',
            self.columnNames['longitude']: 'longitude',
        })

    def getCountries(self):
        dfCountries = self.confirmed.country.unique()
        return dfCountries

    def getByCountryDate(self):
        dfByDate = None
        for col in self.confirmed.columns:
            if col not in self.columns:
                date = datetime.strptime(col, '%m/%d/%y')
                confirmed = self.confirmed[['province', 'country', 'latitude', 'longitude'] + [col]]
                confirmed['date'] = date
                confirmed = confirmed.groupby(['country', 'date']).sum().rename(columns={col: 'confirmed'})
                deaths = self.deaths[['province', 'country'] + [col]]
                deaths['date'] = date
                deaths = deaths.groupby(['country', 'date']).sum().rename(columns={col: 'deaths'})
                recovered = self.recovered[['province', 'country'] + [col]]
                recovered['date'] = date
                recovered = recovered.groupby(['country', 'date']).sum().rename(columns={col: 'recovered'})
                confirmed = confirmed.join(deaths).join(recovered)
                if dfByDate is None:
                    dfByDate = confirmed
                else:
                    dfByDate = concat([dfByDate, confirmed])
        return dfByDate


class CoVid19Database(object):
    def __init__(self):
        super(CoVid19Database, self).__init__()
        self.name = 'covid19'
        self.host = 'localhost'
        self.port = 27017
        self.client = MongoClient(self.host, self.port)
        self.db = self.client[self.name]

    def dropAll(self):
        self.db.countries.drop()
        self.db.countrydays.drop()

    def saveCountries(self, countries):
        for country in countries:
            self.db.countries.insert_one({
                'name': country
            })

    def saveDayByCountry(self, data):
        data = data.reset_index(drop=False)
        self.db.countrydays.insert_many(data.to_dict('records'))


getter = CoVid19Getter()
database = CoVid19Database()
database.dropAll()
database.saveCountries(getter.getCountries())
database.saveDayByCountry(getter.getByCountryDate())
